#include "Image.h"
#include <iostream>

Image::Image() : tab(nullptr), dimx(0), dimy(0) {}

Image::Image(int dimensionX, int dimensionY) : dimx(dimensionX), dimy(dimensionY) {
    tab = new Pixel[dimx * dimy];
    effacer(Pixel());
}

Image::~Image() {
    delete[] tab;
    dimx = dimy = 0;
}

Pixel Image::getPix(int x, int y) {

    if (x >= 0 && x < dimx && y >= 0 && y < dimy)
        return tab[y * dimx + x];
    else
        return Pixel();
}

Pixel Image::getPix(int x, int y) const {

    if (x >= 0 && x < dimx && y >= 0 && y < dimy)
        return tab[y * dimx + x];
    else
        return Pixel();
}

void Image::setPix(int x, int y, Pixel couleur) {

    if (x >= 0 && x < dimx && y >= 0 && y < dimy)
        tab[y * dimx + x] = couleur;
}

void Image::dessinerRectangle(int Xmin, int Ymin, int Xmax, int Ymax, Pixel couleur) {
    for (int i = Xmin; i <= Xmax; ++i) {
        for (int j = Ymin; j <= Ymax; ++j) {
            setPix(i, j, couleur);
        }
    }
}

void Image::effacer(Pixel couleur) {
    for (int i = 0; i < dimx; ++i) {
        for (int j = 0; j < dimy; ++j) {
            setPix(i, j, couleur);
        }
    }
}

void Image::testRegression() {

    Image img(10, 10);

    Pixel p1(255, 0, 0);
    img.setPix(5, 5, p1);

    Pixel p2 = img.getPix(5, 5);
    if (p2.r == 255 && p2.g == 0 && p2.b == 0)
        std::cout << "Test de modification de pixel réussi." << std::endl;
    else
        std::cerr << "Erreur lors du test de modification de pixel." << std::endl;

    img.dessinerRectangle(1, 1, 3, 3, Pixel(0, 255, 0));

    std::cout << "Test de dessin de rectangle réussi." << std::endl;

    img.effacer(Pixel()); // Effacement de l'image
    for (int i = 0; i < 10; ++i) {
        for (int j = 0; j < 10; ++j) {
            Pixel p = img.getPix(i, j);
            if (p.r != 0 || p.g != 0 || p.b != 0) {
                std::cerr << "Erreur lors du test d'effacement de l'image." << std::endl;
                return;
            }
        }
    }

    std::cout << "Test d'effacement de l'image réussi." << std::endl;
}

