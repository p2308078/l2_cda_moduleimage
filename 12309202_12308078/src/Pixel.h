#ifndef _PIXEL_H
#define _PIXEL_H

struct Pixel {
    int r, g, b;

    Pixel() : r(0), g(0), b(0) {}

    Pixel(int nr, int ng, int nb) : r(nr), g(ng), b(nb) {}
};

#endif
