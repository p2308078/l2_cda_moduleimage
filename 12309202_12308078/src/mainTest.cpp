#include <iostream>
#include "Pixel.h"

int main() {

	Pixel pixelNoir;

	std::cout << "Pixel noir - R: " << pixelNoir.r << ", G: " << pixelNoir.g << ", B: " << pixelNoir.b << std::endl;

	Pixel pixelRouge(255, 0, 0);

	std::cout << "Pixel rouge - R: " << pixelRouge.r << ", G: " << pixelRouge.g << ", B: " << pixelRouge.b << std::endl;

	Pixel p1;
	Pixel p2(10, 20, 30);

	std::cout << "Contenu du Pixel 1 - R: " << p1.r << ", G: " << p1.g << ", B: " << p1.b << std::endl;
	std::cout << "Contenu du Pixel 2 - R: " << p2.r << ", G: " << p2.g << ", B: " << p2.b << std::endl;

    return 0;
}

